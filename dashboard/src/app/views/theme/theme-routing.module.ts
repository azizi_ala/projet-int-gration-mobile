import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { PartenairesComponent } from './partenaires.component';
import { JoueursComponent } from './joueurs.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Theme'
    },
    children: [
      {
        path: '',
        redirectTo: 'partenaires'
      },
      {
        path: 'partenaires',
        component: PartenairesComponent,
        data: {
          title: 'Partenaires'
        }
      },
      {
        path: 'joueurs',
        component: JoueursComponent,
        data: {
          title: 'joueurs'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThemeRoutingModule {}
