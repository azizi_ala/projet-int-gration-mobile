import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';

@Component({
  templateUrl: 'joueurs.component.html'
})
export class JoueursComponent implements OnInit {

  playerList: any = [];
  config : any;
 
  
  constructor(private _router : Router,
    private _playerService: ApiService,
    private route : ActivatedRoute) { 

      this.config = {
        currentPage : 1,
        itemPerPage : 10,
        totalItems:0
      }
      route.queryParams.subscribe(
        params =>this.config.currentPage['page'] ? params['page']:1
      )
      for(let i=1; i<=100; i++){

        this.playerList.push(`player ${i}`);
      }
    }
    pageChange(newPage: number){
      this._router.navigate([''], {queryParams: {page: newPage}})
    }

  ngOnInit(){

    this.getPlayer();
  }

  getPlayer(){
    this._playerService. getPlayers().subscribe((data) => {
     this.playerList = data;
    })    
  }
  getArrayFromNumber(length){

    return new Array(length);
  }
}
