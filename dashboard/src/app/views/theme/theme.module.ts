// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// Theme Routing
import { ThemeRoutingModule } from './theme-routing.module';
import { PartenairesComponent } from './partenaires.component';
import { JoueursComponent } from './joueurs.component';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule, TabsModule, CarouselModule, CollapseModule, PaginationModule, PopoverModule, ProgressbarModule, TooltipModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ThemeRoutingModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TabsModule,
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot()
    
  ],
  declarations: [
    PartenairesComponent, 
    JoueursComponent]
    
})
export class ThemeModule { }
