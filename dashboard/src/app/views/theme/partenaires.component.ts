import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';

@Component({
  templateUrl: 'partenaires.component.html'
})
export class PartenairesComponent implements OnInit{
  partnerList: any = [];
  constructor(private _router : Router,
    private _partnerService: ApiService) { }
  ngOnInit(){
    
    this.getPartner();
  }

  getPartner(){
    this._partnerService.getPartners().subscribe((data) => {
     this.partnerList = data;
    })    
  }

}
