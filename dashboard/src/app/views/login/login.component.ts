import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit{ 
loginForm: FormGroup;

  
  constructor(
    private _router: Router,
    private _apiService: ApiService,
    private _activatedRoute: ActivatedRoute
  ){
    this.loginForm = new FormGroup({
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  ngOnInit(){}

  isValid(controlName) {
    return this.loginForm.get(controlName).invalid && this.loginForm.get(controlName).touched;
  }
  login(){
    
    if(this.loginForm.valid){
    this._apiService.login(this.loginForm.value)
    .subscribe(
      data => {
        console.log(data);
        localStorage.setItem('token', data['token'].toString());
        localStorage.setItem('username', data['username'].toString());
        this._router.navigate(['/dashboard']);
      },
      error =>{ 
        if(error.status == 403){
          console.log({error : error})
        }
        if(error.status == 500){
          console.log({error  })
        }
      }
    );
  }

  }
  movetoregister() {
    this._router.navigate(['../register'], {relativeTo: this._activatedRoute});
  }

}
