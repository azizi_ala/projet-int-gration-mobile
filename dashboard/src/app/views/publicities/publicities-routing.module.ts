import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicitiesComponent } from './publicities.component';


const routes: Routes = [
  {
    path: '',
    data : {
      title : 'Publicites'
    },
    children: [
      {
        path: '',
        redirectTo:'publicites'
      },
      {
        path: 'publicites/:id',
        component: PublicitiesComponent,
        data: {
          title: 'publicities-Partenaire'
        }
      },
      {
        path: 'publicites',
        component: PublicitiesComponent,
        data: {
          title: 'Publicites'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicitiesRoutingModule { }
