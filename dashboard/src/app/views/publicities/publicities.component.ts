import { Component, OnDestroy, OnInit } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { MapService } from '../map/map.service';




@Component({
  templateUrl: 'publicities.component.html',

})
export class PublicitiesComponent implements OnInit {
  showPublicities = true
  publiciteList: any = []
  isActive : Boolean;

  constructor(
    private _router: Router,
    private _apiService: ApiService,
    private map: MapService,
    private activatedRoute: ActivatedRoute
  ) {

  }
  Consulter() {
    this.showPublicities = this.showPublicities

  }
  onUpdate(event, pub){
    pub.isActive = !pub.isActive
    console.log(pub)
    this._apiService.updateEvent(pub._id, pub).subscribe((data)=>
    {
      console.log(data)
    })
  }
  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
  console.log(params)
      console.log(params['id'])
      if (params['id'] == "all"){
        this.getPublicite();
      }else{
        
       this.getPublicitebyPartner(params['id'])
       
      }
      //
    });
    



  }
  
  getPublicite() {

    this._apiService.getPublicities().subscribe((data) => {
      this.publiciteList = data;
      for (let i = 0; i < this.publiciteList.length; i++) {
        this._apiService.getPartnerid(this.publiciteList[i]['partner']).subscribe((data) => {

          this.publiciteList[i]['partner'] = data
          console.log(this.publiciteList[i])

        })

      }
      console.log(this.publiciteList)
    }


    );


  }
  getPublicitebyPartner(id) {

    this._apiService.getPublicityByid(id).subscribe((data) => {
      this.publiciteList = data;
      for (let i = 0; i < this.publiciteList.length; i++) {
        this._apiService.getPartnerid(this.publiciteList[i]['partner']).subscribe((data) => {

          this.publiciteList[i]['partner'] = data
          console.log(this.publiciteList[i])

        })

      }
      console.log(this.publiciteList)
    }


    );


  }


}
