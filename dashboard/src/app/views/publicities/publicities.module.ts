import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicitiesRoutingModule } from './publicities-routing.module';
import { PublicitiesComponent } from './publicities.component';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule, TabsModule, CarouselModule, CollapseModule, PaginationModule, PopoverModule, ProgressbarModule, TooltipModule } from 'ngx-bootstrap';
import { SearchPublicitiesPipe } from './search-publicities.pipe';


@NgModule({
  declarations: [PublicitiesComponent, SearchPublicitiesPipe],
  imports: [
    CommonModule,
    PublicitiesRoutingModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TabsModule,
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot()
  ]
})
export class PublicitiesModule { }
