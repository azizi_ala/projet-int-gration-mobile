import { Pipe, PipeTransform } from '@angular/core';
import { publicite } from '../../model/publicity';


@Pipe({
  name: 'searchPublicities'
})
export class SearchPublicitiesPipe implements PipeTransform {

  transform(value: Array<publicite>, args?: any): any {
    if(!args){
      return value;
    }
    args = args.toLowerCase();
    return value.filter((pub => {
      return pub.namePublicity.toLocaleLowerCase().includes(args);
    }));
    return null;
  }

}

