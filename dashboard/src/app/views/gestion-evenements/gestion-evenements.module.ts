import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GestionEvenementsRoutingModule } from './gestion-evenements-routing.module';
import { EventsComponent } from './events.component';
import { SearchEventsPipe } from './search-events.pipe';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule, TabsModule, CarouselModule, CollapseModule, PaginationModule, PopoverModule, ProgressbarModule, TooltipModule } from 'ngx-bootstrap';


@NgModule({
  declarations: [EventsComponent, SearchEventsPipe],
  imports: [
    CommonModule,
    GestionEvenementsRoutingModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TabsModule,
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot()
  ]
})
export class GestionEvenementsModule { }
