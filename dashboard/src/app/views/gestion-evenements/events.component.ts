import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { event } from '../../model/event';
import { ApiService } from '../../services/api.service';




@Component({
  templateUrl: 'events.component.html'
  
})
export class EventsComponent implements OnInit{
  
 eventList: any = [];
 isActive : Boolean;

  constructor(
   private _eventServices: ApiService,
    private _router : Router,
    private activatedRoute: ActivatedRoute
    ) { }

ngOnInit(){
  this.activatedRoute.params.subscribe(params => {

    console.log(params)
   
    if (params['id'] == "allEvent") {
      this.getEvents();
    }else{
      this.getEventbyPartner(params['id'])

    }
    
  });
 // 
}
onUpdate(event, e){
  e.isActive = !e.isActive
  console.log(e)
  this._eventServices.updateEvent(e._id, e).subscribe((data)=>
  {
    console.log(data)
  })
}
  

getEvents(){
  this._eventServices.getEvents().subscribe((data) =>{

  
  this.eventList = data;
  console.log(this.eventList)
  for (let i = 0; i < this.eventList.length; i++) {
    this._eventServices.getPartnerid(this.eventList[i]['partner']).subscribe((data) => {

      this.eventList[i]['partner'] = data
      console.log(this.eventList[i])

    })

  }
  console.log(this.eventList);
});
}


getEventbyPartner(id) {

  this._eventServices.getEventByid(id).subscribe((data) => {
    this.eventList = data;
    for (let i = 0; i < this.eventList.length; i++) {
      this._eventServices.getPartnerid(this.eventList[i]['partner']).subscribe((data) => {

        this.eventList[i]['partner'] = data
        console.log(this.eventList[i])

      })

    }
    console.log(this.eventList)
  }


  );


}

onDelete(event,index): any{
  if(window.confirm('Are you sure?')) {
    this._eventServices.deleteEvent(event._id).subscribe((data) =>{
      this.eventList.splice(index,1);
    });
  }

}



}
