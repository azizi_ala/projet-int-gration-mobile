import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventsComponent } from './events.component';

const routes: Routes = [
  {
    path:'',
    data : {
      title : 'Gestion Evenements'
     
    },
    children: [
      {
        path: '',
        redirectTo:'events',
      },
      {
        path: 'events/:id',
        component: EventsComponent,
        data: {
          title: 'Events-Partenaire'
        }
      },
      {
        path: 'events',
        component: EventsComponent,
        data: {
          title: 'Events'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GestionEvenementsRoutingModule { }
