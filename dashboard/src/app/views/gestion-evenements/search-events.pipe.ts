import { Pipe, PipeTransform } from '@angular/core';
import { event } from '../../model/event';

@Pipe({
  name: 'searchEvents'
})
export class SearchEventsPipe implements PipeTransform {

  transform(value: Array<event>, args?: any): any {
    if(!args){
      return value
    }
    args = args.toLowerCase();
    return value.filter((event => {
      return event.title.toLocaleLowerCase().includes(args);
    }));

    return null;
  }

}
