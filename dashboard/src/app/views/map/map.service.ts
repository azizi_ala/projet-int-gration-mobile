import { Injectable, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { environment } from "../../../environments/environment";
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
@Injectable({
providedIn: 'root'
})
export class MapService implements OnInit{
mapgl: mapboxgl.Map;
marker : mapboxgl.Marker ;
style = 'mapbox://styles/mapbox/streets-v11';
lat = 36.786302;
lng = 10.188446;
zoom = 5;
baseUri:string = "http://localhost:3000";
headers = new HttpHeaders().set('Content-Type', 'application/json');
constructor(private _http: HttpClient) {
  mapboxgl.accessToken = environment.mapbox.accessToken;
}
ngOnInit(){

}
buildMap(l, ln, title, description) {
  this.mapgl = new mapboxgl.Map({
    container: 'map',
    style: this.style,
    zoom: this.zoom,
    center: [this.lng, this.lat],
   
  })
  
 this.mapgl.addControl(new mapboxgl.NavigationControl());
 this.marker =  new mapboxgl.Marker()
 .setLngLat([ln,l])
 .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
 .setHTML('<h3>' + title + '</h3><p>' + description + '</p>'))
 .addTo(this.mapgl);

 
}
/*buildMapFromArray(l, ln, title, description) {
  this.mapgl = new mapboxgl.Map({
    container: 'map',
    style: this.style,
    zoom: this.zoom,
    center: [this.lng, this.lat],
   
  })
  
 this.mapgl.addControl(new mapboxgl.NavigationControl());
 this.marker = new mapboxgl.Marker()
 .setLngLat([ln,l])
 .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
 .setHTML('<h3>' + title + '</h3><p>' + description + '</p>'))
 .addTo(this.mapgl);

 
}*/

}