import { Component, OnInit } from '@angular/core';

import { MapService } from '../map/map.service';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { publicite } from '../../model/publicity';
import { player } from '../../model/player';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  pub: publicite
  id: String;
  players: any = [];
  events: any = [];

  constructor(
    private map: MapService,
    private activatedRoute: ActivatedRoute,
    private _apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {

      console.log(params)
      if (params['id'] === "players") {
        this.getPlays()
      }
      else if (params['id'] === "events") {

        this.getEvents();

      } else {
        this.id = params["id"];
        this.getPub(this.id);
      }
      //
    });
    //get pub par id

  }
  getPlays() {
    this._apiService.getPlayers().subscribe(data => {
      this.players = data
      console.log(this.players)

      for (let i = 0; i < this.players.length; i++) {
        console.log(this.players[i]['location']['coordinates'][1], ' + ',
          this.players[i]['location']['coordinates'][0])
        this.map.buildMap(this.players[i]['location']['coordinates'][1],
          this.players[i]['location']['coordinates'][0],
          this.players[i]['firstName'],
          this.players[i]['email']
        )
      }

    });

  }
  getEvents() {
    this._apiService.getEvents().subscribe(data => {
      this.events = data
      console.log(this.events)

      for (let i = 0; i < this.events.length; i++) {
        console.log(this.events[i]['zone']['coordinates'][1], ' + ',
          this.events[i]['zone']['coordinates'][0])
        this.map.buildMap(this.events[i]['zone']['coordinates'][1],
          this.events[i]['zone']['coordinates'][0],
          this.events[i]['title'],
          this.events[i]['description']
        )
      }





    });

  }

  getPub(id) {
    this._apiService.getPublicityid(id).subscribe(data => {
      this.pub = data
      console.log(this.pub);
      this.map.buildMap(this.pub['location']['coordinates'][1],
        this.pub['location']['coordinates'][0],
        this.pub['namePublicity'],
        this.pub['description']
      )


    });

  }

}
