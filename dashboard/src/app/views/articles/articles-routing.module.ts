import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticlesComponent } from './articles.component';


const routes: Routes = [
  {
    path: '',
    data : {
      title : 'Articles'
    },
    children: [
      {
        path: '',
        redirectTo:'articles'
      },
      {
        path: 'articles/:id',
        component: ArticlesComponent,
        data: {
          title: 'articles-Partenaire'
        }
      },
      {
        path: 'articles',
        component: ArticlesComponent,
        data: {
          title: 'Articles'
        }
      },
     
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlesRoutingModule { }
