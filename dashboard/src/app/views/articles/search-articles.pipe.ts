import { Pipe, PipeTransform } from '@angular/core';
import { article } from '../../model/article';

@Pipe({
  name: 'searchArticles'
})
export class SearchArticlesPipe implements PipeTransform {

  transform(value:Array<article>, args?: any): any {
    if(!args){
      return value;
    }
    args = args.toLowerCase();
    return value.filter((article =>{
      return article.intitule.toLocaleLowerCase().includes(args);
    }));
     
    return null;
  }

}
