import {Component, OnDestroy, OnInit} from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';


@Component({
  templateUrl: 'articles.component.html',
 
})
export class ArticlesComponent implements  OnInit {

  articleList: any = [];
  showArticle = true

  constructor(
    private _router : Router,
    private _articleService: ApiService,
    private activatedRoute: ActivatedRoute
  ) {}
  
  ngOnInit(){
    this.activatedRoute.params.subscribe(params => {

      console.log(params['id'])
      
      if (params['id'] === "allItem") {
        this.getArticles();
        
      }
      else {
       this.getItemPartner(params['id'])
      }
    });
  }
  onUpdate(event, article){
    article.isActive = !article.isActive
    console.log(article)
    this._articleService.updateEvent(article._id, article).subscribe((data)=>
    {
      console.log(data)
    })
  }
   
  getArticles(){
   this._articleService.getArticles().subscribe((data) =>{
    this.articleList = data;
    for (let i = 0; i < this.articleList.length; i++) {
      this._articleService.getPartnerid(this.articleList[i]['partner']).subscribe((data) => {

        this.articleList[i]['partner'] = data
        console.log(this.articleList[i])

      })

    }
    console.log(this.articleList);
   });    
  }
  getItemPartner(id) {

    this._articleService.getItemByid(id).subscribe((data) => {
      this.articleList = data;
      for (let i = 0; i < this.articleList.length; i++) {
        this._articleService.getPartnerid(this.articleList[i]['partner']).subscribe((data) => {

          this.articleList[i]['partner'] = data
          console.log(this.articleList[i])

        })

      }
      console.log(this.articleList)
    }


    );


  }
}

