import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { User } from '../../model/user';
import { partner } from '../../model/partner';

@Component({
  templateUrl: 'utilisateurs.component.html'
})
export class UtiisateurComponent implements OnInit{
  partnerList: any = [];
  playerList: any = [];
  constructor(private _router: Router,
    private _apiService: ApiService,
    private _activatedRoute: ActivatedRoute) { }
    
    
  ngOnInit(){
    this.getPartners();
    this.getPlayers();
   
    
  }
  onUpdate(event, user){
    user.status = !user.status
    console.log(user)
     if(user.role == 'partner'){
      this._apiService.updatePartner(user._id, user).subscribe((data)=>
      {
        console.log(data)
      })
     }else {
      this._apiService.updatePlayer(user._id, user).subscribe((data)=>
      {
        console.log(data)
      })
     }
    
  }

  getPartners(){
    this._apiService.getPartners().subscribe((data) => {
     this.partnerList = data;
    })    
  }
  getPlayers(){
    this._apiService. getPlayers().subscribe((data) => {
     this.playerList = data;
    })    
  }


}
