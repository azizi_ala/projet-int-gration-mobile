import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GestionUtilisateursRoutingModule } from './gestion-utilisateurs-routing.module';
import { UtiisateurComponent } from './utilisateurs.component';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule, TabsModule, CarouselModule, CollapseModule, PaginationModule, PopoverModule, ProgressbarModule, TooltipModule } from 'ngx-bootstrap';


@NgModule({
  declarations: [UtiisateurComponent],
  imports: [
    CommonModule,
    GestionUtilisateursRoutingModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TabsModule,
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot()
  ]
})
export class GestionUtilisateursModule { }
