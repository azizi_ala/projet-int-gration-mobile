import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UtiisateurComponent } from './utilisateurs.component';


const routes: Routes = [
  {
    path: '',
    data : {
      title : 'Gestion Utilisateur'
    },
    children: [
      {
        path: '',
        redirectTo:'utilisateurs'
      },
      {
        path: 'utilisateurs',
        component: UtiisateurComponent,
        data: {
          title: 'Activate'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GestionUtilisateursRoutingModule { }
