import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit{

  successMessage = '';
  myForm: FormGroup;
  constructor(private _apiService: ApiService,
    private _router: Router) { 
    this.myForm = new FormGroup({
      email: new FormControl(null, Validators.email),
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      cnfpassword: new FormControl(null, this.passValidator)
    });
    this.myForm.controls.password.valueChanges
    .subscribe(
      x => this.myForm.controls.cnfpassword.updateValueAndValidity()
    );
  }
  ngOnInit(){}

  isValid(controlName) {
    return this.myForm.get(controlName).invalid && this.myForm.get(controlName).touched;
  }

  passValidator(control: AbstractControl){
    if (control && (control.value !== null || control.value !== undefined)) {
      const cnfpassValue= control.value;
      const passControl = control.root.get('password');
            if(passControl) {
              const passValue = passControl.value;
              if(passValue !== cnfpassValue || passValue ===''){
                return {
                  isError: true
                };
              }
            }
    }
    return null;
  }
  register(){
    console.log(this.myForm.value);
    if(this.myForm.valid){
    this._apiService.submitRegister(this.myForm.value)
    .subscribe(
      data => {
        this.successMessage = "Registration Success";
        this._router.navigate(['/login']);
        
      },
      error => {
        this.successMessage = 'Some error';
        if(error.status == 403){
          console.log({error : error})
        }
        if(error.status == 500){
          console.log({error  })
        }
      
      }
    );   
      }
    }
}

