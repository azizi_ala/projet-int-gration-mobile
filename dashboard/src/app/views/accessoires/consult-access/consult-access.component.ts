import { Component, OnInit } from '@angular/core';
import {AccessoiresService  } from '../../../services/accessoires.service';
@Component({
  selector: 'app-consult-access',
  templateUrl: './consult-access.component.html',
  styleUrls: ['./consult-access.component.css']
})
export class ConsultAccessComponent implements OnInit {


 
accessoireList: any = []; 


  constructor(private _apiService: AccessoiresService) { 
      }

  ngOnInit(){
    this.getAccessoires();
  }

  getAccessoires(){
    this._apiService.getaccessoires().subscribe((data) =>
      this.accessoireList = data);
      console.log(this.accessoireList);
   }

 

  removeEmployee(accessory, index) {
    if(window.confirm('Are you sure?')) {
        this._apiService.delteaccessoire(accessory._id).subscribe((data) => {
          this.accessoireList.splice(index, 1);
        }
      )    
    }
  }

}
