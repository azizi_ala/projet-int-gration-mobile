import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultAccessComponent } from './consult-access.component';

describe('ConsultAccessComponent', () => {
  let component: ConsultAccessComponent;
  let fixture: ComponentFixture<ConsultAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
