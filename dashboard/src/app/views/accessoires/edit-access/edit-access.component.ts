import { Accessoire } from '../../../model/accessoire';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { AccessoiresService } from '../../../services/accessoires.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";


@Component({
  selector: 'app-edit-accessess',
  templateUrl: './edit-access.component.html',
  styleUrls: ['./edit-access.component.css']
})
export class EditAccessComponent implements OnInit {

  submitted = false;
  editForm: FormGroup;
  employeeData: Accessoire[];
  EmployeeProfile: any = ['Finance', 'BDM', 'HR', 'Sales', 'Admin'];
 


  constructor(
    public fb: FormBuilder,
    private actRoute: ActivatedRoute,
    private apiService: AccessoiresService,
    private router: Router
  ) { }

  ngOnInit() {
    this.updateEmployee();
    let id = this.actRoute.snapshot.paramMap.get('id');
    this.getEmployee(id);
    this.editForm = this.fb.group({
      title: ['', [Validators.required]],
      intitule: ['', [Validators.required]],
      price: ['', [Validators.required]],
      image: ['', [Validators.required]]


    })
  }

  // Choose options with select-dropdown
  updateProfile(e) {
    this.editForm.get('designation').setValue(e, {
      onlySelf: true
    })
  }

  // Getter to access form control
  get myForm() {
    return this.editForm.controls;
  }

  getEmployee(id) {
    this.apiService.getaccessoireid(id).subscribe(data => {
     
      this.editForm.setValue({
        
        title: data['title'],
        intitule: data['intitule'],
        price: data['price'],
        image: data['image']


      });
      console.log(data);
    });
  }

  updateEmployee() {
    this.editForm = this.fb.group({
      title: ['', [Validators.required]],
      intitule: ['', [Validators.required]],
      price: ['', [Validators.required]],
      image: ['', [Validators.required]]




    })
  }

  onSubmit() {
    this.submitted = true;
    if (!this.editForm.valid) {
      return false;
    } else {
      if (window.confirm('Are you sure?')) {
        let id = this.actRoute.snapshot.paramMap.get('id');
        this.apiService.updateaccessoire(id, this.editForm.value)
          .subscribe(res => {
            this.router.navigateByUrl('/accessoires/accessoiresconsulter');
            console.log('Content updated successfully!')
          }, (error) => {
            console.log(error)
          })
      }
    }
  }
}
