import { Pipe, PipeTransform } from '@angular/core';
import { Accessoire } from '../../model/accessoire';

@Pipe({
  name: 'searchAccessoire'
})
export class SearchAccessoirePipe implements PipeTransform {

  transform(value: Array<Accessoire>, args?: any): any {
    if(!args){
      return value
    }
    args = args.toLowerCase();
    return value.filter((accessoire => {
      return accessoire.title.toLocaleLowerCase().includes(args);
    }));
    return null;
  }

}
