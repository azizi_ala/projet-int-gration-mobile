import { Component, OnInit, NgZone, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import {AccessoiresService  } from '../../services/accessoires.service';
import { HttpEvent, HttpEventType, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';  
import { catchError, map } from 'rxjs/operators';  

@Component({
  templateUrl: 'accessoires.component.html'
})
export class AccessoiresComponent implements OnInit{
  imageUrl : string ='assets/img/avatars/default-img.jpeg'; 
  fileToUpload : File = null ;

  constructor(private _apiService: AccessoiresService, private router: Router){}
   
  ngOnInit(){}
  handleFileInput(file : FileList){
    this.fileToUpload = file.item(0);
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }
  OnSubmit(intitule,title,image, price, startDate, endDate){

    this._apiService.addAccessoire(intitule.value, title.value, this.fileToUpload, price.value,startDate.value, endDate.value).subscribe(
      response => {
        intitule.value = null;
        title.value = null;
        image.value =null;
        price.value = null;
        endDate.value = null;
        this.router.navigate(['/accessoires/accessoiresconsulter']);
        
      }, (error) => {
        if(error.status == 403){
          console.log({error : error})
        }
        if(error.status == 500){
          console.log({error  })
        }
   
      });
    
  }  
  
   
}
