import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessoiresRoutingModule } from './accessoires-routing.module';
import { AccessoiresComponent } from './accessoires.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule, TabsModule, CarouselModule, CollapseModule, PaginationModule, PopoverModule, ProgressbarModule, TooltipModule } from 'ngx-bootstrap';
import { ConsultAccessComponent } from './consult-access/consult-access.component';
import { EditAccessComponent } from './edit-access/edit-access.component';
import { SearchAccessoirePipe } from './search-accessoire.pipe';


@NgModule({
  declarations: [AccessoiresComponent, ConsultAccessComponent, EditAccessComponent, SearchAccessoirePipe],
  imports: [
    CommonModule,
    AccessoiresRoutingModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TabsModule,
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),
    ReactiveFormsModule
  ]
})
export class AccessoiresModule { }
