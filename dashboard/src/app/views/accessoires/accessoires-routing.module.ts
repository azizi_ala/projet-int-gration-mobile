import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessoiresComponent } from './accessoires.component';
import { ConsultAccessComponent } from './consult-access/consult-access.component';
import { EditAccessComponent } from './edit-access/edit-access.component';


const routes: Routes = [
    {
      path: '',
      data : {
        title : 'gestion Accessoires'
      },
    children: [
      {
        path: '',
        redirectTo:'accessoires'
      },
      {
        path: 'adds',
        component: AccessoiresComponent,
        data: {
          title: 'ajout Accessoires'
        }
      },
      {
        path: 'accessoiresconsulter',
        component: ConsultAccessComponent,
        data: {
          title: 'consulter Accessoires '
        }
      },
      {
        path: 'accessoiresedit/:id',
        component: EditAccessComponent,
        data: {
          title: 'edit Accessoires '
        }
      }
    ]
  }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessoiresRoutingModule { }
