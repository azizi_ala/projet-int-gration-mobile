import {Component, OnInit } from '@angular/core';
import { navItems } from '../../_nav';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit{
  public sidebarMinimized = false;
  public navItems = navItems;

   toggleMinimize(e) {
    this.sidebarMinimized = e;
   }
   username = '';
   constructor(
    private _apiService: ApiService,
    private _router: Router)
  {
  
  }
  ngOnInit(){
    /*this._apiService.getUserName()
    .subscribe(
       data => this.username = data.toString(),
       error => this._router.navigate(['/login'])
    );*/
    console.log(localStorage.getItem('username'))
    this.username = localStorage.getItem('username')
  }
  logout(){
    localStorage.removeItem('token');
    this._router.navigate(['/login']);
  }
}
