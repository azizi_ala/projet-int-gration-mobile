import { Injectable } from '@angular/core';
import { Observable, throwError, from } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Accessoire } from '../model/accessoire';

@Injectable({
  providedIn: 'root'
})
export class AccessoiresService {

  accessory: Array<Accessoire> = [];
  baseUri: string = "http://localhost:3000";
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }



  addAccessoire(intitule: string, title: string, fileToUpload: File, price: number, startDate: Date, endDate: Date): Observable<any> {
    var formData: FormData = new FormData();
    formData.append("intitule", intitule);
    formData.append("title", title);
    formData.append("image", fileToUpload, fileToUpload.name);
    formData.append("price", String(price));
    formData.append("endDate", String(endDate));
   
    return this.http.post<Accessoire>(`${this.baseUri}/accessory/addAccessory`, formData, {
      reportProgress: true,
      observe: 'events'
    })
  }


  // Get accessoir
  getaccessoireid(id): Observable<any> {
    let url = `${this.baseUri}/accessory/${id}`;
    return this.http.get(url, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {}
      }),
      catchError(this.errorMgmt)
    )
  }



  getaccessoires() {
    return this.http.get('http://localhost:3000/accessories');
  }
 
  // Update accessoire
  updateaccessoire(id, data): Observable<any> {
    let url = `${this.baseUri}/updateAccessory/${id}`;
    return this.http.patch(url, data, { headers: this.headers }).pipe(
      catchError(this.errorMgmt)
    )
  }

  // Delete accesoire
  delteaccessoire(id): Observable<any> {
    let url = `${this.baseUri}/removeAccessory/${id}`;
    return this.http.delete(url, { headers: this.headers }).pipe(
      catchError(this.errorMgmt)
    )
  }




  // Error handling 
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
