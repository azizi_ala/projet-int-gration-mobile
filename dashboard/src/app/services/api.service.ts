import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { event } from '../model/event';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { publicite } from '../model/publicity';
import { player } from '../model/player';
import { partner } from '../model/partner';


@Injectable({
  providedIn: 'root'
})
export class ApiService {


  event: Array<event> = [];
  publicite: Array<publicite> = [];
  player: Array<player> = [];
  partner: Array<partner> = [];

  baseUri: string = "http://localhost:3000";
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private _http: HttpClient) { }



  submitRegister(body: any) {
    return this._http.post('http://localhost:3000/admin/register', body, {
      observe: 'body'
    });
  }

  login(body: any) {
    return this._http.post('http://localhost:3000/admin/login', body, {
      observe: 'body'
    });

  }
  getUserName() {
    return this._http.get('http://localhost:3000/admin/username');
  }


  getEvents() {
    return this._http.get('http://localhost:3000/events');
  }
  addEvent(event: any): any {
    this.event.push(event);
    return this._http.post('http://localhost:3000/addEvent', event, { observe: 'response' });
  }

  // Delete employee
  deleteEvent(id: any): Observable<any> {
    let url = `${this.baseUri}/removeEvent/${id}`;
    return this._http.delete(url, { headers: this.headers }).pipe(
      catchError(this.errorMgmt)
    )
  }

  getPublicities() {
    return this._http.get('http://localhost:3000/publicities')
  }
  //Get id by Partner
  getPublicityByid(id) {
    let url = `${this.baseUri}/publicities/${id}`;
    return this._http.get(url, { headers: this.headers })
    
  }
  // Get publicity
  getPublicityid(id): Observable<any> {
    let url = `${this.baseUri}/publicity/${id}`;
    return this._http.get(url, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {}
      }),
      catchError(this.errorMgmt)
    )
  }
  updatePublicity(id, data): Observable<any> {
    let url = `${this.baseUri}/updatePublicity/${id}`;
    return this._http.patch(url, data, { headers: this.headers }).pipe(
      catchError(this.errorMgmt)
    )
  }

  getPartnerid(id): Observable<any> {
    let url = `${this.baseUri}/partner/${id}`;
    return this._http.get(url, { headers: this.headers }).pipe(
      map((res: Response) => {
        return res || {}
      }),
      catchError(this.errorMgmt)
    )
  }

  getPlayers() {
    return this._http.get('http://localhost:3000/players');
  }
  getUser() {
    return this._http.get('http://localhost:3000/getUser');
  }
  getPartners() {
    return this._http.get('http://localhost:3000/partners');
  }
  getArticles(){
    return this._http.get('http://localhost:3000/items');
  }
  updateItem(id, data): Observable<any> {
    let url = `${this.baseUri}/updateItem/${id}`;
    return this._http.patch(url, data, { headers: this.headers }).pipe(
      catchError(this.errorMgmt)
    )
  }
  getItemByid(id) {
    let url = `${this.baseUri}/items/${id}`;
    return this._http.get(url, { headers: this.headers })
    
  }
  updateEvent(id, data): Observable<any> {
    let url = `${this.baseUri}/updateEvent/${id}`;
    return this._http.patch(url, data, { headers: this.headers }).pipe(
      catchError(this.errorMgmt)
    )
  }
  getEventByid(id) {
    let url = `${this.baseUri}/events/${id}`;
    return this._http.get(url, { headers: this.headers })
    
  }

  updatePartner(id, data): Observable<any> {
    let url = `${this.baseUri}/updatePartner/${id}`;
    return this._http.patch(url, data, { headers: this.headers }).pipe(
      catchError(this.errorMgmt)
    )
  }
  updatePlayer(id, data): Observable<any> {
    let url = `${this.baseUri}/updatePlayer/${id}`;
    return this._http.patch(url, data, { headers: this.headers }).pipe(
      catchError(this.errorMgmt)
    )
  }


  // Error handling 
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
