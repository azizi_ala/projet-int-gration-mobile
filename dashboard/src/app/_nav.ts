import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
  },
  {
    title: true,
    name: 'Map'
  },
  {
    name: 'Map',
    url: '/map',
    icon: 'icon-drop'
  },
  {
    name: 'Partenaires',
    url: '/theme/partenaires',
    icon: 'icon-drop'
  },
  {
    name: 'Joueurs',
    url: '/theme/joueurs',
    icon: 'icon-pencil'
  },
  {
    title: true,
    name: 'Gestion'
  },
  {
    name: 'Gestion Utilisateurs',
    url: '/utilisateurs',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Utilisateurs',
        url: '/utilisateurs/utilisateurs',
        icon: 'icon-puzzle'
      }
    ]
  },
  {
    name: 'Evenements',
    url: '/events/allEvent',
    icon: 'icon-puzzle'
  },
  {
    name: 'Publicites',
    url: '/publicites/all',
    icon: 'icon-drop'
  },
  {
    name: 'Articles',
    url: '/articles/allItem',
    icon: 'icon-puzzle'
  },
  
  {
    name: 'Accessoires',
    url: '/accessoires',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Ajout Accessoires',
        url: '/accessoires/adds',
        icon: 'icon-puzzle'
      },
      {
        name: 'consulter Accessoires',
        url: '/accessoires/accessoiresconsulter',
        icon: 'icon-puzzle'
      }
    ]
  },  
  {
    divider: true
  }
];
