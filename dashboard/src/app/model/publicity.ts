import { partner } from './partner';

export class publicite {
    
    namePublicity : string;
    description :string;
    startDate:Date;
    partner: partner;
    location : string;
    isActive: Boolean;
    
    
}