import { partner } from './partner';

export class event {
    
    title : string;
    description :string;
    numberParticipants: number;
    partner: partner;
    maxNumberParticipant: number;
    endDate: Date;
    isActive : Boolean;
   

}