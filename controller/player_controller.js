const Player = require ('../model/player_model.js'); // inclut le schéma de 
const router = require('express').Router();
const cors = require('cors');
const bcrypt = require('bcryptjs');
router.use(cors());

router.addPlayer = async (req, res, next) => {
   

    const emailExist = await Player.findOne({ email: req.body.email });
    if (emailExist) return res.status(400).send('Email already exists ');

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);


    try {
        const p = new Player({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            dateOfBirth: req.body.dateOfBirth,
            age: req.body.age,
            phoneNumber: req.body.phoneNumber,
            email: req.body.email,
            coins: 0,
            gems : 0,
            role: 'player',
            adress: req.body.adress,
            password: hashPassword,
            status: false
    
        });
        const player = await Player.create(p);
        return res.status(200).json({
            success: true,
            data: player
        });
       
    } catch (err){
        console.error(err);
        if(err.code === 11000) {
            return res.status(400).json({ error: 'This player alrady exist'});
        }
        res.status(500).json({error : 'server Error'})
    }
}
router.login = (req, res, next) => {
    //checks that email is present or not
    let data = {
        id: '',
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        coins: '',
        gems : '',
    }
    Player.findOne({ 'email': req.body.email }, (err, user) => {

        if (!user){ res.json({message :'Login failed, user not found'});
            console.log("failed...")
    }
    

        else {
             //if email is present then it will compare password
            // console.log(user)
            user.comparePassword(req.body.password, (err, isMatch) => {
                if (err) throw err;
                if (!isMatch) return res.status(400).json({
                    message: 'wrong Password'
                });

                data.id = user._id;
                data.firstName = user.firstName;
                data.lastName = user.lastName;
                data.email = user.email;
                data.password = user.password;
                data.coins = user.coins;
                data.gems = user.gems;

                //    return res.status(200).json(data);
                res.status(200).json({
                    success: true,
                    data
                });

                console.log(user)
            })
        }
    })
}
 
 router.listPlayer = (req, res, next) => {
    Player.find() //fetches all the posts
       .then(result => {
           res.send(result);
       }).catch(err => {
           res.status(400).send(err);
       })
};
router.singlePlayer = (req, res, next) => {
    Player.findById(req.params.id) //filters the posts by Id
       .then(result => {
           res.send(result);
       }).catch(err => {
           res.status(400).send(err);
       })
};
router.updatePlayer = (req, res) => {
    var id = req.params.id;
    Player.update({_id: id}, req.body)
    .then(player=> {
        if(!player) res.json({success :false, result: "player does not exist"})
        
        res.json(player)
    })
    .catch(err => {
        res.json({ success: false, result: err})
    })
};
router.deletePlayer = (req, res, next) => {
    Player.findByIdAndRemove(req.params.id)
      .then(()=>{
          res.send('Player deleted');
      })
      .catch(err => res.status(400).send(err));
};
module.exports = router;